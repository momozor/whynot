whynot
------
A super-tiny Lisp compiler.

[![asciicast](https://asciinema.org/a/Xx5rl3xtwV5uV6Y7pNN1Njv7Q.svg)](https://asciinema.org/a/Xx5rl3xtwV5uV6Y7pNN1Njv7Q)

Basic Progress
---------
* [x] [Lexical Analysis]
* [x] [Abstract Syntax Tree assembling]
* [ ] [Intermediate Representation of C++17 (IR)]
* [ ] [Runtime checks of syntactic issues]
