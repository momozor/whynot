#!/usr/bin/env python3


from typing import List, Dict
from re import match
from sys import argv, exit
import pprint
import logging

try:
    SOURCE_FILE: str = argv[1]
except IndexError:
    logging.error(
        'You must include source input file for the first command line argument'
    )
    logging.warning('Usage: python3 wn.py ./source_code.wnlisp')
    exit(1)


SOURCE_CODE: str = ''
with open(SOURCE_FILE, 'r') as file:
    SOURCE_CODE = file.read()


def deoparen(source_code: str) -> str:
    return source_code.replace('(', ' ( ')


def decparen(deoparenized: str) -> List[str]:
    return deoparenized.replace(')', ' ) ').split()


def tokenizer(splitted_code: List[str]) -> List[Dict[str, str]]:
    grammar_list: List[Dict[str, str]] = []
    reserved_keywords: List[str] = ['progn', 'def', '+', '*']

    for i in splitted_code:
        if i == '(':
            grammar_list.append({'type': 'oparen', 'value': i})
            
        if match('[def|progn|+|\*]', i):
            grammar_list.append({'type': 'special-form', 'value': i})

        if i == ')':
            grammar_list.append({'type': 'cparen', 'value': i})

        if match('[0-9]', i):
            grammar_list.append({'type': 'number', 'value': i})

        if match('[a-zA-Z]', i) and i not in reserved_keywords:
            grammar_list.append({'type': 'symbol', 'value': i})

    return grammar_list


def atomize(token: Dict[str, str]):
    if match('[0-9]', token['value']):
        new_token = token
        new_token['value'] = int(token['value'])
        return new_token
    else:
        return token


def ast_assembler(source_tokens: List[Dict[str, str]]):
    if len(source_tokens) == 0:
        raise SyntaxError('unexpected EOF')
    initial_token: Dict[str, str] = source_tokens.pop(0)

    if initial_token['value'] == '(' and initial_token['type'] == 'oparen':
        new_stack_frame: List = []

        while source_tokens[0]['value'] != ')' and initial_token['type'] != 'cparen':
            new_stack_frame.append(ast_assembler(source_tokens))
        source_tokens.pop(0)

        return new_stack_frame
    elif initial_token['value'] == ')' and initial_token['type'] == 'cparen':
        raise SyntaxError('unexpected )')
    else:
        return atomize(initial_token)


def generic_parser(source_code: str) -> List:
    return ast_assembler(tokenizer(decparen(deoparen(source_code))))

def semantic_symbol_table(ast: List):
    pp = pprint.PrettyPrinter(indent=2)
    print('\n\n\n')

    lookup_table: List = []
    ntable: List = []
    main_token = ast[0]
    if main_token['type'] == 'special-form' and main_token['value'] == 'progn':
        pp.pprint(ast[1:])
        lookup_table.append(ast[1:][0])

def main():
    tokens: List[Dict[str, str]] = tokenizer(decparen(deoparen(SOURCE_CODE)))
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(tokens)
    print("Total Characters: {}".format(len(tokens)))
    parsed = generic_parser(SOURCE_CODE)
    pp.pprint(parsed)
    semantic_symbol_table(parsed)


if __name__ == '__main__':
    main()
